import React, { Component } from 'react';

import './style.scss';

export default class Post extends Component {
  render() {
    let content = this.props.content
    
    if(Array.isArray(content))
      content = content.map((body) => <p>{body}</p>)
    else
      content = <p>{content}</p>

    return (
    <section className={this.props.highlight ? "border-and-shadow highlight" : "border-and-shadow"}>
        <h1>{this.props.title}</h1>
        {content}
    </section>
    );
  }
}
