import React, { Component } from 'react';

import logoName from '../../img/logo-name.svg';
import './style.scss';

export default class Menu extends Component {
  render() {
    let listItems = [<li><img src={logoName}></img></li>]
    let textItems = this.props.items.map( (item, index) => <li key={index}><a href="#">{item}</a></li>)
    listItems.push(...textItems)
    console.log(listItems)
    return (
      <nav>
        <menu className="border-and-shadow">
            <ul>
                {/* <li></li> */}
                {/* <img src={logoName}></img> */}
                {listItems}
            </ul>
        </menu>
        {/* <menu className="border-and-shadow"></menu>
        <menu className="border-and-shadow"></menu>
        <menu className="border-and-shadow"></menu>
        <menu className="border-and-shadow"></menu> */}
      </nav>
    );
  }
}
