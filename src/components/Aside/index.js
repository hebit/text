import React, { Component } from 'react';

import './style.scss';

export default class Aside extends Component {
  render() {
    return (
        <aside className="border-and-shadow">
          <h2>Searching for:</h2>
          <ul>
            <li><a href="#s01">countries that u should meet</a></li>
            <li><a href="#s02">11 babies drinking water</a></li>
            <li><a href="#s03">what's the life's reason</a></li>
            <li><a href="#s04">how to take a shower</a></li>
            <li><a href="#s05">hey that my number</a></li>
          </ul>
          <div  className="aside-input-box">
            <span className="big-text">?</span>
            <div>
              <span className="or">or</span>
              no, i'm searching
              <div>
                <label for="search">for:</label>
                <input id="search" placeholder="Type here..."></input>
              </div>
            </div>
          </div>
        </aside>
    );
  }
}
