import React, { Component } from 'react';
import variables from './variables.scss';

import Header from './components/Header';
import Menu from './components/Menu';
import Post from './components/Post';
import Aside from './components/Aside';

import './style.scss';

class App extends Component {
  // https://jsonplaceholder.typicode.com/posts

  state = {
    posts: []
  }

  componentDidMount = () => {
    fetch('https://jsonplaceholder.typicode.com/posts').then(
      res => res.json().then(
        data => {
          data = data.slice(0,5);
          data[1].highlight = true;
          console.log(data)
          this.setState({posts: data})
        }
      )
    )
    // var themes = JSON.parse(variables.content.replace(/\\a/g, '').replace(/\'/g, ''))
    var themes = variables.content.replace(/\\a/g, '').replace(/\'/g, '')
    console.log(themes)
    var myObj = JSON.parse('{"p": 5}');
    console.log(myObj);
  }

  render() {
    return (
      <>
        <Header />
        <Menu items={['texts', 'photographs', 'mood', 'color:']} />
        <main>
          {this.state.posts.map(post => <Post title={post.title} content={post.body} highlight={post.highlight} />)}

        </main>
        <Aside>

        </Aside>
      </>
    );
  }
}

export default App;
